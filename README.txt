RiseupVPN
-----------------------

Anonymous VPN. Easy, fast, secure.

This repo has everything needed to build RiseupVPN on different platforms
(windows, mac, linux).

RiseupVPN is a branded build of Bitmask Lite, written in go.


Dependencies
------------------------

* golang
* make

Dependencies (Windows)
------------------------

* nsis
* nssm



