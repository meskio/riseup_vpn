!include "MUI.nsh"

Name "$app_name"
RequestExecutionLevel admin

Outfile $app_name-installer.exe
;TODO make the installdir configurable - and set it in the registry.
InstallDir "C:\Program Files\$app_name\"


!define BITMAP_FILE riseupvpn.bmp
!define MUI_PAGE_CUSTOMFUNCTION_PRE WelcomePageSetupLinkPre
!define MUI_PAGE_CUSTOMFUNCTION_SHOW WelcomePageSetupLinkShow

; TODO fixme icons PATH
!define MUI_ICON "..\assets\$app_name_lower.ico"
!define MUI_UNICON "..\assets\$app_name_lower.ico"

!define MUI_WELCOMEPAGE_TITLE "$app_name"
!define MUI_WELCOMEPAGE_TEXT "This will install $app_name in your computer. $app_name is a simple VPN Client, powered by Bitmask."
#!define MUI_WELCOMEFINISHPAGE_BITMAP "riseup.png"

!insertmacro MUI_PAGE_WELCOME
#!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
 
 
Function WelcomePageSetupLinkPre
  File "..\assets\$app_name_lower.png"
  File "..\assets\$app_name_lower.ico"

  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Settings" "Numfields" "4" ; increase counter
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 3" "Bottom" "122" ; limit size of the upper label
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Type" "Link"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Text" "$url"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "State" "$url"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Left" "120"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Right" "315"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Top" "123"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Bottom" "132"
FunctionEnd
 
Function WelcomePageSetupLinkShow
  ; Thanks to pengyou
  ; Fix colors of added link control
  ; See http://forums.winamp.com/showthread.php?s=&threadid=205674
  Push $0
 
  GetDlgItem $0 $MUI_HWND 1203
  SetCtlColors $0 "0000FF" "FFFFFF"
  ; underline font
  CreateFont $1 "$(^Font)" "$(^FontSize)" "400" /UNDERLINE 
  SendMessage $0 ${WM_SETFONT} $1 1 
  Pop $0
 
FunctionEnd

Section "InstallFiles"
  SetOutPath $INSTDIR 
  WriteUninstaller $INSTDIR\uninstaller.exe

  File "readme.txt"
  File "..\staging\nssm.exe"

  $extra_install_files
SectionEnd

Section "InstallService"
  ; TODO: make sure that this is not running before attempting
  ; to install it.
  ExecWait '"$INSTDIR\nssm.exe" install $app_name_lower-helper "$INSTDIR\bitmask_helper.exe"'
  ExecWait '"$INSTDIR\nssm.exe" set $app_name_lower-helper AppDirectory "$INSTDIR"'
  ExecWait '"$INSTDIR"\nssm.exe start $app_name_lower-helper'
  ; Exec cmd.exe

SectionEnd

Section "Uninstall"
  ExecWait '"$INSTDIR"\nssm.exe stop $app_name_lower-helper'
  ExecWait '"$INSTDIR"\nssm.exe remove $app_name_lower-helper'

  Delete $INSTDIR\uninstaller.exe
  Delete $INSTDIR\readme.txt
  Delete $INSTDIR\nssm.exe

  $extra_uninstall_files
SectionEnd
 
;----------------------------------------
;Languages
 
!insertmacro MUI_LANGUAGE "English"
