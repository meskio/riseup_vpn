!include "MUI.nsh"

Name "RiseupVPN"
RequestExecutionLevel admin

Outfile RiseupVPN-installer.exe
;TODO make the installdir configurable - and set it in the registry.
InstallDir "C:\Program Files\RiseupVPN\"


!define BITMAP_FILE riseupvpn.bmp
!define MUI_PAGE_CUSTOMFUNCTION_PRE WelcomePageSetupLinkPre
!define MUI_PAGE_CUSTOMFUNCTION_SHOW WelcomePageSetupLinkShow

; TODO fixme icons PATH
!define MUI_ICON "..\assets\riseupvpn.ico"
!define MUI_UNICON "..\assets\riseupvpn.ico"

!define MUI_WELCOMEPAGE_TITLE "RiseupVPN"
!define MUI_WELCOMEPAGE_TEXT "This will install RiseupVPN in your computer. RiseupVPN is a simple VPN Client, powered by Bitmask."
#!define MUI_WELCOMEFINISHPAGE_BITMAP "riseup.png"

!insertmacro MUI_PAGE_WELCOME
#!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
 
 
Function WelcomePageSetupLinkPre
  File "..\assets\riseupvpn.png"
  File "..\assets\riseupvpn.ico"

  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Settings" "Numfields" "4" ; increase counter
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 3" "Bottom" "122" ; limit size of the upper label
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Type" "Link"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Text" "https://riseup.net"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "State" "https://riseup.net"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Left" "120"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Right" "315"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Top" "123"
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Field 4" "Bottom" "132"
FunctionEnd
 
Function WelcomePageSetupLinkShow
  ; Thanks to pengyou
  ; Fix colors of added link control
  ; See http://forums.winamp.com/showthread.php?s=&threadid=205674
  Push $0
 
  GetDlgItem $0 $MUI_HWND 1203
  SetCtlColors $0 "0000FF" "FFFFFF"
  ; underline font
  CreateFont $1 "$(^Font)" "$(^FontSize)" "400" /UNDERLINE 
  SendMessage $0 ${WM_SETFONT} $1 1 
  Pop $0
 
FunctionEnd

Section "InstallFiles"
  SetOutPath $INSTDIR 
  WriteUninstaller $INSTDIR\uninstaller.exe

  File "readme.txt"
  File "..\staging\nssm.exe"

  
  File ..\staging\bitmask_helper.exe
  File ..\staging\bitmask-systray.exe
  File ..\staging\openssl.exe
  File ..\staging\openvpn.exe
  File ..\staging\ssleay32.dll
  File ..\staging\libeay32.dll
  File ..\staging\liblzo2-2.dll
  File ..\staging\libpkcs11-helper-1.dll

SectionEnd

Section "InstallService"
  ; TODO: make sure that this is not running before attempting
  ; to install it.
  ExecWait '"$INSTDIR\nssm.exe" install riseupvpn-helper "$INSTDIR\bitmask_helper.exe"'
  ExecWait '"$INSTDIR\nssm.exe" set riseupvpn-helper AppDirectory "$INSTDIR"'
  ExecWait '"$INSTDIR"\nssm.exe start riseupvpn-helper'
  ; Exec cmd.exe

SectionEnd

Section "Uninstall"
  ExecWait '"$INSTDIR"\nssm.exe stop riseupvpn-helper'
  ExecWait '"$INSTDIR"\nssm.exe remove riseupvpn-helper'

  Delete $INSTDIR\uninstaller.exe
  Delete $INSTDIR\readme.txt
  Delete $INSTDIR\nssm.exe

  
  Delete $INSTDIR\bitmask_helper.exe
  Delete $INSTDIR\bitmask-systray.exe
  Delete $INSTDIR\openssl.exe
  Delete $INSTDIR\openvpn.exe
  Delete $INSTDIR\ssleay32.dll
  Delete $INSTDIR\libeay32.dll
  Delete $INSTDIR\liblzo2-2.dll
  Delete $INSTDIR\libpkcs11-helper-1.dll

SectionEnd
 
;----------------------------------------
;Languages
 
!insertmacro MUI_LANGUAGE "English"
